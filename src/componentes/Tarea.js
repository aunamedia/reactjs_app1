import React, { Component } from "react";
import PropTypes from "prop-types";

import "./Tarea.css";

export default class Tarea extends Component {

    EstiloCompletado(){
        return {
            fontSize: '20px',
            color: this.props.tarea.done?'gray':'black',
            textDecoration: this.props.tarea.done ? 'line-through' :'none'
        }
    }
    render(){
        const {tarea} = this.props
        return <p style={this.EstiloCompletado()}>
            
        {tarea.titulo} - {tarea.descripcion} - {tarea.done}
        <input type="checkbox" onChange={this.props.onCambiarEstado.bind(this, tarea.id)}></input>
        <button style={btnBorrar} onClick={this.props.onDelete.bind(this, tarea.id)}>x</button>
        
            </p>
    }
}

Tarea.propTypes = {
    tarea: PropTypes.object.isRequired
}

const btnBorrar = {
    fontSize: '18px',
    background: '#ea2027',
    color: '#fff',
    border: 'none',
    padding: '10px 15px',
    borderRadius: '50%',
    cursor: 'pointer'
}

