import React, { Component } from "react";

export default class TareaForm extends Component {

    state = {
        titulo: '',
        descripcion: ''
    }

    enviar = e => {
        this.props.addTarea(this.state.titulo, this.state.descripcion)
        e.preventDefault()
    }

    cambio = e => {
        this.setState({[e.target.name]:e.target.value})
    }

    render(){
        return (
            <form onSubmit={this.enviar}>
                <input type="text" 
                    name="titulo"
                    placeholder="Escribe una tarea" 
                    onChange={this.cambio} 
                    value={this.state.titulo} />
                <br />
                <br />
                <textarea 
                    name="descripcion"
                    placeholder="Escribe un comentario"  
                    onChange={this.cambio} 
                    value={this.state.descripcion}/>
                <br />
                <br />
                <input type="submit" />
            </form>
        )
    }
}