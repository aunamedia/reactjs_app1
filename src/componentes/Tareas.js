import React, { Component } from "react";
import PropTypes from "prop-types";

// Components
import Tarea from "./Tarea";

class Tareas extends Component {
    render(){
        return this.props.tareas.map(e=>
        <Tarea 
            key={e.id} 
            tarea={e} 
            onDelete={this.props.onDelete}
            onCambiarEstado={this.props.onCambiarEstado}
            />
        )
    }
}

Tareas.propTypes = {
    tareas: PropTypes.array.isRequired
}


export default Tareas;