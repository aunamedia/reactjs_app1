import React, { Component } from 'react'

export default class Posts extends Component {

    state = {
        posts : []
    }

    async componentDidMount(){
        const url = 'https://jsonplaceholder.typicode.com/posts'
        const res = await fetch(url)
        const data = await res.json();
        this.setState({posts: data})
    }

    render() {
        return (
            <div>
                <h1>POSTS</h1>
                {
                    this.state.posts.map(post => {
                        return <div key={post.id}>
                            <h2>{post.id}. {post.title}</h2>
                            <h5>{post.body}</h5>
                            </div>
                    })
                }
            </div>
        )
    }
}

