import React, {Component} from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


import tareas from './datos/tareas.json'

//Componentes
import Tareas from "./componentes/Tareas.js";
import TareaFrom from "./componentes/TareaFrom";
import Posts from './componentes/Posts'

class App extends Component {

  state = {
    tareas: tareas
  }

  addTarea = (tit, desc) => {
    const nuevaTarea = {
      titulo: tit,
      descripcion: desc,
      done: false,
      id: this.state.tareas.length
    }
    this.setState({
      tareas: [...this.state.tareas, nuevaTarea]
    })
    console.log("AGREGANDO..."+tit+desc)
  }

  deleteTarea = (id) => {
    const nuevaT = this.state.tareas.filter(tarea => tarea.id !== id)
        this.setState({
          tareas: nuevaT
        })
  }

  cambiarEstado = (id) => {
    const nT = this.state.tareas.map(tarea => {
      if(tarea.id === id) {
        tarea.done = !tarea.done
      }
      return tarea
    })
    this.setState({
      tareas: nT
    })
  }

  render(){
    return <div>
      <Router>

        <Link to="/"> Tareas</Link> - 
        <Link to="/posts"> Posts</Link>

      <Route exact path="/" render={() => {
        return <div>
          <TareaFrom addTarea={this.addTarea}/>
          <Tareas tareas={this.state.tareas} 
            onDelete={this.deleteTarea}
            onCambiarEstado={this.cambiarEstado}/>
        </div>
      }} />

      <Route path="/posts" component={Posts} />

      </Router>

    </div>
  }
}

export default App;
